INTRODUCTION
============
- OpenStack is a subsidiary module of Cloud module, therefore this module does
  NOT work without Cloud module.

- See also [Cloud](https://drupal.org/project/cloud/) module.

REQUIREMENTS
============

- `Drupal 8.8.x` or higher (The latest version of Drupal 8 or 9)
- `Cloud 8.x-2.x` or higher

INSTALLATION
============
- `composer require drupal/cloud`

Active Maintainers
==================

- `yas` (https://drupal.org/u/yas)
- `baldwinlouie` (https://www.drupal.org/u/baldwinlouie)
- `xiaohua-guan` (https://www.drupal.org/u/xiaohua-guan)
- `Masami` (https://www.drupal.org/u/Masami)
- `Jigish Chauhan` (https://www.drupal.org/u/jigishaddweb)
